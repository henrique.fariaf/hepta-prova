# Tecnologias Utilizadas

- Java 11
- Maven
- Eclipse 
- Tomcat 9

# Configurando o Banco de dados

- Use o Mysql 8
- Crie um banco de dados "mercado" (Utilize a porta 3306)
- Configure um usuário de name="root" e senha="root
- O hibernate está pronto para gerar o banco de dados

**OBS** - Ao executar o teste de inserir fabricantes o hibernate irá criar as tabelas.

# Rodando os testes

- Rode o comando ``` mvn test -Dtest=InserirFabricanteTest ``` para adicionar inserir dois fabricantes para realizar os testes com os produtos
- Rode a aplicação (O projeto deve estar rodando na porta 8080 do localhost)
- Rode o comando ``` mvn test ```


# Build da aplicação

- Rode o comando```mvn clean package```
- Será gerado o war com o nome "mercado-0.0.1-SNAPSHOT.war" 
- Renomeie para "mercado.war"

# Rodando aplicação

- Faça o deploy do war no servidor Tomcat 9
- Acesse o endereço: localhost:8080/mercado

