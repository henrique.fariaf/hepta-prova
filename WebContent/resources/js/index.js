let inicio = new Vue({
	el: "#inicio",
	data: {
		listaProdutos: [],
		fabricantes: [],
		listaProdutosHeader: [
			{ sortable: true, key: "nome", label: "Nome" },
			{ sortable: true, key: "fabricante.nome", label: "Fabricante" },
			{ sortable: true, key: "volume", label: "Volume" },
			{ sortable: true, key: "unidade", label: "Unidade" },
			{ sortable: true, key: "estoque", label: "Estoque" },
			{ key: 'acoes', label: 'Ações', sortable: false },
		],
		novoProduto: {
			nome: '',
			fabricante: { id: 1},
			volume: 0.0,
			unidade: '',
			estoque: 0,
		},
		nome: 'Teste',
		edicao: false,
		idEdicao: null,
		selected: null,
		mostrarModal: false,
	},
	created: function () {
		let vm = this;
		vm.buscaProdutos();
		vm.buscaFabricantes();
	},
	computed: {
		fabricanteOpcoes() {
			return this.fabricantes.map(F => {
				return { value: F.id, text: F.nome }
			})
		},
		formValido(){
			return !!this.novoProduto.nome 
				&& !!this.novoProduto.fabricante.id
				&& !!this.novoProduto.volume
				&& !!this.novoProduto.unidade
				&& !!this.novoProduto.estoque
		}
	},
	methods: {
		buscaProdutos: function () {
			const vm = this;
			axios.get("/mercado/rs/produtos")
				.then(response => {
					vm.listaProdutos = response.data;
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi possível listar os serviços");
				}).finally(function () {
					console.log('dados carregados', vm.listaProdutos)
				});

		},
		buscaFabricantes: function () {
			const vm = this;
			axios.get("/mercado/rs/fabricantes")
				.then(response => {
					vm.fabricantes = response.data;
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi possível listar os Fabricantes");
				}).finally(function () {
					console.log('dados carregados', vm.fabricantes)
				});
		},
		onCancelarClick(){
			this.mostrarModal = false;
			this.limparForm();
		},
		limparForm(){
			this.mostrarModal = false;
			this.novoProduto.nome = '';
			this.novoProduto.fabricante.id = 1;
			this.novoProduto.volume = 0.0;
			this.novoProduto.unidade = '';
			this.novoProduto.estoque = 0;

		},
		onEditClick(produto) {
			this.selected = produto && produto.fabricante.id;

			this.idEdicao = produto && produto.id;

			this.edicao = !this.edicao;
		},
		mostrar(produto) {
			let isId = produto.id == this.idEdicao && this.edicao;
			return isId;
		},
		onUpdateClick(produto) {
			const vm = this;
			produto.fabricante = { id: this.selected, nome: produto.fabricante.nome }
			axios.put(`/mercado/rs/produtos/${produto.id}`, produto).finally(function () {
				vm.onEditClick(null);
				vm.buscaProdutos();
			})
		},
		onSalvarClick(){
			const vm = this;
			axios.post(`/mercado/rs/produtos`, this.novoProduto).then(function(){
				alert(`Produto ${vm.novoProduto.nome} criado com sucesso`)}).catch(function(){
					alert(`Houve um erro`)
				}).finally(function(){
					vm.limparForm();
					vm.buscaProdutos();
				})
		},

		onDeleteClick(produto) {
			this.idDelecao = produto.id;
			const vm = this;
			if (confirm('Tem certeza que deja excluir este produto?'))
				axios.delete(`/mercado/rs/produtos/${produto.id}`).finally(function () {
					vm.buscaProdutos();
				})
		}
	}
});