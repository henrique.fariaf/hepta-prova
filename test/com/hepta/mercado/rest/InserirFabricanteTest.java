package com.hepta.mercado.rest;

import org.junit.jupiter.api.Test;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.persistence.FabricanteDAO;

public class InserirFabricanteTest {

	@Test
	public void inserirFabricantes() throws Exception {
		
		Fabricante fabricante = new Fabricante();
		fabricante.setNome("Nike");
		FabricanteDAO dao = new FabricanteDAO();
		dao.save(fabricante);
		
		fabricante = new Fabricante();
		fabricante.setNome("Adidas");
		dao.save(fabricante);
				
	}
		
}
