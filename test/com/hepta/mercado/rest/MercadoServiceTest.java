package com.hepta.mercado.rest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.ProdutoDAO;

class MercadoServiceTest {

	private WebTarget service;
	private final String URL_LOCAL = "http://localhost:8080/mercado/rs/produtos";

	@BeforeEach
	void setUpBeforeClass() throws Exception {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		this.service = client.target(UriBuilder.fromUri(URL_LOCAL).build());
	}

	@Test
	void testListaTodosProdutos() {
		// QUANDO
		Response response = service.request().get();
		// ENTAO
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}

	@Test
	void testCriarProduto() {
		Produto novoProduto = new Produto();
		novoProduto.setNome("Produto de Teste");
		novoProduto.setEstoque(10);
		Fabricante fabricante = new Fabricante();
		fabricante.setId(1);
		novoProduto.setFabricante(fabricante);
		String unidade = ("ml");
		novoProduto.setUnidade(unidade);
		Double volume = (220.6);
		novoProduto.setVolume(volume);

		// QUANDO
		Response response = service.request().post(Entity.entity(novoProduto, MediaType.APPLICATION_JSON));
		// ENTAO
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}

	@Test
	void testUpdateProduto() throws Exception {

		ProdutoDAO dao = new ProdutoDAO();
		List<Produto> produtos = dao.getAll();

		Collections.shuffle(produtos);
		assert (produtos.size() > 0);
		Produto produto = produtos.get(0);
		produto.setNome("O nome foi alterado (Teste)");

		// QUANDO
		Response response = service.path(produto.getId().toString()).request()
				.put(Entity.entity(produto, MediaType.APPLICATION_JSON));
		// ENTAO
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}
	@Test
	void testDeleteProduto() throws Exception {

		ProdutoDAO dao = new ProdutoDAO();
		List<Produto> produtos = dao.getAll();

		Collections.shuffle(produtos);
		assert (produtos.size() > 0);
		Produto produto = produtos.get(0);
		
		// QUANDO
		Response response = service.path(produto.getId().toString()).request()
				.delete();
		// ENTAO
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}


}
